const addAjaxPagination = (jqForm, container) => {
    container.find('a[href*="/ajax/"]').on('click', e => {
        e.preventDefault();

        const href = $(e.target).attr('href');
        const pageRegEx = /page=(\d+)/gi;
        const pageNr = pageRegEx.exec(href)[1];

        handleAjaxFilter(jqForm, pageNr);
    });
};

const handleAjaxFilter = (jqForm, pageNr = 1) => {
    const url = jqForm.attr('action');
    let params = jqForm.serialize();
    if (pageNr > 1) {
        params += '&page=' + pageNr;
    }
    const filterContainer = $('#' + jqForm.data('filter-container'));

    const handleSuccess = (data, textStatus, jqXHR) => {
        filterContainer.html(data);
        addAjaxPagination(jqForm, filterContainer);
        contentWayPoint();
    };

    const useGet = false; // for debugging purposes
    useGet ? $.get(url, params, handleSuccess) : $.post(url, params, handleSuccess);
};

const initAjaxFilter = () => {
    $('#ajaxCategoryFilter input:checkbox').on('change', (e) => {
        const jqForm = $(e.target.closest('form'));
        handleAjaxFilter(jqForm);
    });
};

initAjaxFilter();