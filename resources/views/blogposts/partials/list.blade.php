<div class="row pt-md-4">
    <div id="blogpost" class="col-md-12">
        @foreach ($blogposts as $blogpost)
            <div class="blog-entry d-md-flex ftco-animate">
                <a href="{{ $blogpost->getPath() }}" class="img img-2"
                    style="background-image: url({{ $blogpost->getImageAsset() }});"></a>
                <div class="text text-2 pl-md-4">
                    <h3 class="mb-2">
                        @if ($blogpost->subscription_only)
                            <i class="icon-paypal mr-2 @if($blogpost->user->subscribers->contains(Auth::user()))
                                text-success
                            @else
                                text-danger
                            @endif "></i>
                        @endif
                        <a href="{{ $blogpost->getPath() }}">{{ $blogpost->title }}</a>
                    </h3>

                    @include('blogposts.partials.writer-buttons')

                    <div class="meta-wrap">
                        <p class="meta">
                            <span><i class="icon-calendar mr-2"></i>{{ $blogpost->created_at }}</span>
                            <span>
                                @foreach ($blogpost->categories as $category)
                                    <a href="{{ $category->getPath() }}"><i
                                            class="icon-folder-o mr-2"></i>{{ $category->title }}</a>
                                @endforeach
                            </span>
                            <span><i class="icon-comment2 mr-2"></i>{{ $blogpost->comments_count }}
                                Comment{{ $blogpost->comments_count === 1 ? '' : 's' }}</span>
                        </p>
                    </div>
                    <p class="mb-4">{{ $blogpost->content }}</p>
                    <p>
                        @can('view', $blogpost)
                            <a href="{{ $blogpost->getPath() }}" class="btn-custom">Read More <span
                                    class="ion-ios-arrow-forward"></span></a>
                        @else
                            <a href="{{ route('subscriptions.create', $blogpost->user) }}"><i class="icon-cc-paypal"></i>
                                Subscribe for premium content <span class="ion-ios-arrow-forward"></span></a>
                        @endcan

                    </p>
                </div>
            </div>
        @endforeach
    </div>
</div><!-- END-->
