@can(['update', 'delete'], $blogpost)
    <div class="mb-3">
        <a class="btn btn-sm btn-warning" href="{{ route('blogposts.edit', $blogpost) }}">Edit</a>
        <form class="d-inline" action="{{ route('blogposts.delete', $blogpost) }}" method="post">
            @csrf
            @method('delete')
            <button class="btn btn-sm btn-danger">Delete</button>
        </form>
    </div>
@endcan
