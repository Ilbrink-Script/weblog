@csrf

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group">
    <label for="title" @error('title') class="text-danger" @enderror>Title</label>
    <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title"
        value="{{ old('title', optional($blogpost)->title) }}" maxlength="255">
    @error('title')
    <div class="invalid-feedback">{{ $errors->first('title') }}</div>
    @enderror
</div>
<div class="form-group">
    <label for="image" @error('image') class="text-danger" @enderror>Image</label>
    @if (optional($blogpost)->image)
        <div class="mb-3"><img src="{{ $blogpost->getImageAsset() }}" alt="" class="w-50"></div>
    @endif
    <input type="file" class="form-control-file @error('image') is-invalid @enderror" id="image" name="image">
    @error('image')
    <div class="invalid-feedback">{{ $errors->first('image') }}</div>
    @enderror
</div>
<div class="form-group">
    <label for="body" @error('body') class="text-danger" @enderror>Blog</label>
    <textarea class="form-control @error('body') is-invalid @enderror" name="body" id="body"
        rows="10">{{ old('body', optional($blogpost)->body) }}</textarea>
    @error('body')
    <div class="invalid-feedback">{{ $errors->first('body') }}</div>
    @enderror
</div>
<div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="subscription_only" name="subscription_only" @if (old('subscription_only', optional($blogpost)->subscription_only))
    checked @endif>
    <label for="subscription_only" class="form-check-label">Subscription only</label>
</div>
<div class="form-group">
    <label @error('categories') class="text-danger" @enderror>Categories</label>
    <div>
        @forelse ($categories as $i => $category)
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="categories[]" id="category{{ $i }}"
                    value="{{ $category->id }}" @if ($blogpost->categories->contains($category)) checked @endif>
                <label class="form-check-label" for="category{{ $i }}">{{ $category->title }}</label>
            </div>
        @empty
            <span class="text-warning">There are no categories available yet.</span>
        @endforelse
    </div>
    @error('categories')
    <div class="invalid-feedback">{{ $errors->first('categories') }}</div>
    @enderror
</div>
