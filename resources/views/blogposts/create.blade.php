@extends('layouts.andrea.app')

@section('content')
    <div class="col-xl-8 py-5 px-md-5">
        <div class="row pt-md-4">
            <div class="col-md-12">
                <form action="{{ route('blogposts.store') }}" method="post" enctype="multipart/form-data">
                    @include('blogposts.partials.form-fields')
                    <div class="form-group">
                        <input type="submit" value="Post Blog" class="btn py-3 px-4 btn-primary">
                    </div>
                </form>
            </div>
        </div><!-- END-->
    </div>
@endsection
