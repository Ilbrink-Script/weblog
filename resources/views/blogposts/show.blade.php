@extends('layouts.andrea.app')

@section('content')
    <div class="col-lg-8 px-md-5 py-5">
        <div class="row pt-md-4">
            <!--row-->
            <h1 class="mb-3">
                @if ($blogpost->subscription_only)
                    <i class="icon-paypal mr-2"></i>
                @endif
                {{ $blogpost->title }}
            </h1>

            <div class="p-2">
                @include('blogposts.partials.writer-buttons')
            </div>

            <div class="meta-wrap">
                <p class="meta">
                    <span><i class="icon-calendar mr-2"></i>{{ $blogpost->created_at }}</span>
                    <span>
                        @foreach ($blogpost->categories as $category)
                            <a href="{{ $category->getPath() }}"><i
                                    class="icon-folder-o mr-2"></i>{{ $category->title }}</a>
                        @endforeach
                    </span>
                </p>
            </div>

            <p><img src="{{ $blogpost->getImageAsset() }}" alt="" class="img-fluid"></p>
            <p>{{ $blogpost->body }}</p>

            @if ($blogpost->categories_count > 0)
                <div class="tag-widget post-tag-container mb-5 mt-5">
                    <div class="tagcloud">
                        @foreach ($blogpost->categories as $category)
                            <a href="{{ $category->getPath() }}" class="tag-cloud-link">{{ $category->title }}</a>
                        @endforeach
                    </div>
                </div>
            @endif

            <div class="about-author d-flex p-4 bg-light">
                <div class="bio mr-5">
                    <a href="{{ route('users.show', $blogpost->user) }}">
                        <img src="{{ asset('img/andrea/person_1.jpg') }}" alt="{{ $blogpost->user->name }}"
                            class="img-fluid mb-4">
                    </a>
                </div>
                <div class="desc">
                    <h3><a href="{{ route('users.show', $blogpost->user) }}">{{ $blogpost->user->name }}</a></h3>
                    <p>{ @todo } Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem
                        necessitatibus
                        voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique,
                        inventore eos fugit cupiditate numquam!</p>
                </div>
            </div>

            @include('comments.index')
        </div><!-- END-->
    </div>
@endsection
