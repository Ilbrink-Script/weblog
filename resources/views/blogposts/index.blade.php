@extends('layouts.andrea.app')

@section('content')
    <div id="blogpost-container" class="col-xl-8 py-5 px-md-5">
        @include('blogposts.partials.list')
        {{ $blogposts->links('layouts.andrea.partials.pagination') }}
    </div>
@endsection
