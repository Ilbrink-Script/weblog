<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    {{--
    <meta name="viewport" content="width=device-width, initial-scale=1"> --}}

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    {{--
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}

    <!-- Styles -->
    {{--
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/andrea.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>

<body>

    <div id="colorlib-page">
        <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
        <aside id="colorlib-aside" role="complementary" class="js-fullheight">
            @include('layouts.andrea.partials.navigation')

            @include('layouts.andrea.partials.footer')
        </aside> <!-- END COLORLIB-ASIDE -->
        <div id="colorlib-main">
            <section class="ftco-section ftco-no-pt ftco-no-pb">
                <div class="container">
                    <div class="row d-flex">
                        @yield('content')

                        @include('layouts.andrea.partials.sidebar')
                    </div>
                </div>
            </section>
        </div><!-- END COLORLIB-MAIN -->
    </div><!-- END COLORLIB-PAGE -->

    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00" /></svg></div>

    <script src="{{ asset('js/andrea/jquery.min.js') }}"></script>
    <script src="{{ asset('js/andrea/jquery-migrate-3.0.1.min.js') }}"></script>
    <script src="{{ asset('js/andrea/popper.min.js') }}"></script>
    <script src="{{ asset('js/andrea/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/andrea/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('js/andrea/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('js/andrea/jquery.stellar.min.js') }}"></script>
    <script src="{{ asset('js/andrea/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/andrea/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('js/andrea/aos.js') }}"></script>
    <script src="{{ asset('js/andrea/jquery.animateNumber.min.js') }}"></script>
    <script src="{{ asset('js/andrea/scrollax.min.js') }}"></script>
    {{-- <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    --}}
    {{-- <script src="{{ asset('js/andrea/google-map.js') }}"></script>
    --}}
    <script src="{{ asset('js/andrea/main.js') }}"></script>
    @yield('pagescript')
</body>

</html>
