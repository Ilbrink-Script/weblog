<nav id="colorlib-main-menu" role="navigation">
    <ul>
        <li @if (Request::routeIs('blogposts.index')) class="colorlib-active" @endif>
            <a href="{{ route('blogposts.index') }}">Home</a>
        </li>
        @auth
            <li @if (Request::routeIs('users.show') && $user->is(Auth::user())) class="colorlib-active" @endif>
                <a href="{{ route('users.show', Auth::user()) }}">{{ Auth::user()->name }}</a></li>
            @if (Auth::user()->has('subscriptions'))
                <li><a href="{{ route('subscriptions.index') }}">My subscriptions</a></li>
            @endif
            @can('create', App\Models\Blogpost::class)
                <li @if (Request::routeIs('blogposts.create')) class="colorlib-active" @endif>
                    <a href="{{ route('blogposts.create') }}">New blogpost</a>
                </li>
            @endcan
            <li @if (Request::routeIs('logout')) class="colorlib-active" @endif>
                <form action="{{ route('logout') }}" method="post" class="d-inline">
                    @csrf
                    {{-- <button class="btn btn-link">Logout BTN</button>
                    --}}
                    <a href="#" onclick="this.closest('form').submit();return false;">Logout</a>
                </form>
            </li>
        @else
            <li @if (Request::routeIs('login')) class="colorlib-active" @endif>
                <a href="{{ route('login') }}">Login</a>
            </li>
            <li @if (Request::routeIs('register')) class="colorlib-active" @endif>
                <a href="{{ route('register') }}">Register</a>
            </li>
        @endauth
    </ul>
</nav>
