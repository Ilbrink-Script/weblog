@if ($paginator->lastPage() > 1)
    <div class="row">
        <div class="col">
            <div class="block-27">
                <ul>
                    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                        @if ($i == $paginator->currentPage())
                            <li class="active"><span>{{ $i }}</li>
                        @else
                            <li><a href="{{ $paginator->url($i) }}">{{ $i }}</a></li>
                        @endif
                    @endfor
                </ul>
            </div>
        </div>
    </div>
@endif
