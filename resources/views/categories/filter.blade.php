@if ($categories->isNotEmpty())
    <div class="sidebar-box ftco-animate">
        <h3 class="sidebar-heading">Categories Filter</h3>
        <form id="ajaxCategoryFilter" action="{{ route('categories.ajax') }}" method="post"
            data-filter-container="blogpost-container">
            @csrf
            @foreach ($categories as $i => $category)
                <div class="form-group form-check">
                    <input class="form-check-input" type="checkbox" name="categories[]" id="category{{ $i }}"
                        value="{{ $category->slug }}">
                    <label class="form-check-label" for="category{{ $i }}">{{ $category->title }}</label>
                </div>
            @endforeach
        </form>
    </div>

    @section('pagescript')
        <script src="{{ asset('/js/ajax-filter.js') }}" defer></script>
    @endsection
@endif
