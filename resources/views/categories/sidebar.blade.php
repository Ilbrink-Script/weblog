@if ($categories->isNotEmpty())
    <div class="sidebar-box ftco-animate">
        <h3 class="sidebar-heading">Categories</h3>
        <ul class="categories">
            @foreach ($categories as $category)
                <li><a href="{{ $category->getPath() }}">{{ $category->title }}<span>({{ $category->blogposts_count }})</span></a></li>
            @endforeach
        </ul>
    </div>
@endif
