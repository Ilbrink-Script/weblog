@extends('layouts.andrea.app')

@section('content')
    <div class="col-xl-8 py-5 px-md-5">
        <div class="row pt-md-4">
            <div class="col-md-12">
                <h2>My subscriptions</h2>
                <ul>
                    @forelse ($subscriptions as $subscription)
                        <li class="clearfix">
                            <a href="{{ route('users.show', $subscription) }}">{{ $subscription->name }}</a>
                            <form class="d-inline float-right" action="{{ route('subscriptions.delete', $subscription) }}" method="post">
                                @method('delete') @csrf
                                <button class="btn btn-link text-danger"><i class="icon-trash"></i> Unsubscribe</button>
                            </form>
                        </li>
                    @empty
                        <li>No subscriptions yet.</li>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
@endsection
