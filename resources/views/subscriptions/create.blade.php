@extends('layouts.andrea.app')

@section('content')
    <div class="col-xl-8 py-5 px-md-5">
        <div class="row pt-md-4">
            <div class="col-md-12">
                <h2 class="mb-3">Want to read premium content by {{ $author->name }}?</h2>
                <form action="{{ route('subscriptions.store', $author) }}" method="post">
                    @csrf
                    <div class="form-group">
                        <input type="submit" value="Subscribe to {{ $author->name }}" class="btn py-3 px-4 btn-primary">
                    </div>
                </form>
            </div>
        </div><!-- END-->
    </div>
@endsection
