<div class="pt-5 mt-5">
    @if ($blogpost->comments_count > 0)
        <h3 class="mb-5 font-weight-bold">{{ $blogpost->comments_count }}
            Comment{{ $blogpost->comments_count > 0 ? 's' : '' }}</h3>
        <ul id="comments" class="comment-list">
            @foreach ($blogpost->comments as $comment)
                <li class="comment" id="comment-{{ $comment->id }}">
                    <div class="vcard bio">
                        <a href="{{ route('users.show', $comment->user) }}">
                            <img src="{{ asset('img/andrea/person_1.jpg') }}" alt="{{ $comment->user->name }}">
                        </a>
                    </div>
                    <div class="comment-body">
                        <h3><a href="{{ route('users.show', $comment->user) }}">{{ $comment->user->name }}</a>
                        </h3>
                        <div class="meta">{{ $comment->created_at }}</div>
                        <p>{{ $comment->body }}</p>
                        <p class="d-inline">
                            <a href="#comment-form" class="reply">Reply</a>
                        </p>
                        @can('delete', $comment)
                            <form action="{{ route('comments.delete', $comment) }}" method="post" class="d-inline">
                                @csrf
                                @method('delete')
                                <button class="btn btn-sm btn-danger">Delete</button>
                            </form>
                        @endcan
                    </div>
                </li>
            @endforeach

        </ul>
        <!-- END comment-list -->
    @else
        <h3 class="mb-5 font-weight-bold">No comments yet</h3>
    @endif

    @can('create', 'App\\Models\\Comment')
        @include('comments.create')
    @else
        <h3 class="mb-5 font-weight-bold">You have to login before posting a comment.</h3>
        @include('auth.partials.login-form')
    @endcan
</div>
