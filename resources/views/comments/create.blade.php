<div class="comment-form-wrap pt-5">
    <h3 class="mb-5">Leave a comment</h3>
    <form id="comment-form" action="{{ route('comments.store', $blogpost) }}" method="post" class="p-3 p-md-5 bg-light">
        @csrf
        {{-- <div class="form-group">
            <label for="name">Name *</label>
            <input type="text" class="form-control" id="name">
        </div> --}}
        {{-- <div class="form-group">
            <label for="email">Email *</label>
            <input type="email" class="form-control" id="email">
        </div> --}}
        {{-- <div class="form-group">
            <label for="website">Website</label>
            <input type="url" class="form-control" id="website">
        </div> --}}

        @error('user_id')
            <div class="text-danger mb-3">You need to be logged in before being able to comment.</div>
        @enderror

        <div class="form-group">
            <label for="body" @error('body') class="text-danger" @enderror>Message</label>
            <textarea name="body" id="body" cols="30" rows="10" class="form-control @error('body') is-invalid @enderror">{{ old('body') }}</textarea>
            @error('body')
                <div class="invalid-feedback">{{ $errors->first('body') }}</div>
            @enderror
        </div>
        <div class="form-group">
            <input type="submit" value="Post Comment" class="btn py-3 px-4 btn-primary">
        </div>

    </form>
</div>
