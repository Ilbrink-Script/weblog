@component('mail::message')
# Weekly weblog digest

Hello {{ $user->name }},

Here's an overview of what you've might have missed this past week

@foreach ($blogposts as $i => $blogpost)

@if($i > 0)
---
@endif

## [{{ $blogpost->title }}]({{ route('blogposts.show', $blogpost) }})

{{ $blogpost->created_at }} by [{{ $blogpost->user->name }}]({{ route('users.show', $blogpost->user) }})

![Image]({{ asset($blogpost->getImageAsset()) }} "{{ $blogpost->title }}")

[Read more]({{ route('blogposts.show', $blogpost) }})
@endforeach

@component('mail::button', ['url' => route('blogposts.index'), 'color' => 'success'])
Read much more
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
