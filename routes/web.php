<?php

use App\Http\Controllers\BlogpostController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\UserController;
use App\Mail\BlogpostDigest;
use App\Models\Blogpost;
use App\Models\User;
use Illuminate\Support\Facades\{Auth,Route};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// blogposts
Route::get('blogposts', [BlogpostController::class, 'index'])->name('blogposts.index');

Route::get('blogposts/create', [BlogpostController::class, 'create'])->name('blogposts.create');
Route::post('blogposts', [BlogpostController::class, 'store'])->name('blogposts.store');

Route::get('blogposts/{blogpost}', [BlogpostController::class, 'show'])->name('blogposts.show');
Route::get('blogposts/{blogpost}/edit', [BlogpostController::class, 'edit'])->name('blogposts.edit');
Route::put('blogposts/{blogpost}', [BlogpostController::class, 'update'])->name('blogposts.update');
Route::delete('blogposts/{blogpost}', [BlogpostController::class, 'destroy'])->name('blogposts.delete');

// categories
Route::get('categories/{category}', [CategoryController::class, 'show'])->name('categories.show');

// ajax categories
Route::any('ajax/categories', [CategoryController::class, 'ajax'])->name('categories.ajax');

// comments
Route::post('comments/{blogpost}', [CommentController::class, 'store'])->name('comments.store');
Route::put('comments/{comment}', [CommentController::class, 'update'])->name('comments.update');
Route::delete('comments/{comment}', [CommentController::class, 'destroy'])->name('comments.delete');

// users
Route::get('users', [UserController::class, 'index'])->name('users.index');
Route::get('users/{user}', [UserController::class, 'show'])->name('users.show');

// subscriptions
Route::get('subscriptions', [SubscriptionController::class, 'index'])->name('subscriptions.index')->middleware('auth');
Route::get('subscriptions/{author}', [SubscriptionController::class, 'create'])->name('subscriptions.create');
Route::post('subscriptions/{author}', [SubscriptionController::class, 'store'])->name('subscriptions.store');
Route::delete('subscriptions/{author}', [SubscriptionController::class, 'destroy'])->name('subscriptions.delete');

// mailable preview
Route::get('mailable', function() {
    $user = User::where('email', 'guest@example.com')->first();
    $blogposts = Blogpost::orderBy('created_at', 'desc')->take(3)->get();

    return new BlogpostDigest($user, $blogposts);
});

// site index
Route::redirect('/', 'blogposts');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
