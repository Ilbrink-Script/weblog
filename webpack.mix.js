const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/ajax-filter.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .styles([
        'resources/andrea/css/animate.css',
        'resources/andrea/css/aos.css',
        'resources/andrea/css/flaticon.css',
        'resources/andrea/css/icomoon.css',
        'resources/andrea/css/ionicons.min.css',
        'resources/andrea/css/magnific-popup.css',
        'resources/andrea/css/open-iconic-bootstrap.min.css',
        'resources/andrea/css/owl.carousel.min.css',
        'resources/andrea/css/owl.theme.default.min.css',
        'resources/andrea/css/style.css',
    ], 'public/css/andrea.css')
    // .js([
    //     'resources/andrea/js/jquery.min.js',
    //     'resources/andrea/js/jquery-migrate-3.0.1.min.js',
    // ], 'public/js/andrea-jq.js')
    // .js([
    //     'resources/andrea/js/popper.min.js',
    //     'resources/andrea/js/bootstrap.min.js',
    //     'resources/andrea/js/jquery.easing.1.3.js',
    //     'resources/andrea/js/jquery.waypoints.min.js',
    //     'resources/andrea/js/jquery.stellar.min.js',
    //     'resources/andrea/js/owl.carousel.min.js',
    //     'resources/andrea/js/jquery.magnific-popup.min.js',
    //     'resources/andrea/js/aos.js',
    //     'resources/andrea/js/jquery.animateNumber.min.js',
    //     'resources/andrea/js/scrollax.min.js',
    //     'resources/andrea/js/google-map.js',
    //     'resources/andrea/js/main.js',
    // ], 'public/js/andrea.js')
    .copyDirectory('resources/andrea/js', 'public/js/andrea')
    .copyDirectory('resources/andrea/images', 'public/img/andrea')
    .copyDirectory('resources/andrea/fonts', 'public/fonts');
