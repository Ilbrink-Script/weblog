<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserRole;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRoles = UserRole::all();

        $paul = new User();
        $paul->password = bcrypt('script123');
        $paul->username = 'Paul';
        $paul->name = 'Paul Ilbrink';
        $paul->email = 'paul@example.com';
        $paul->save();

        // I'm admin
        $paul->userRoles()->attach($userRoles->firstWhere('slug', 'admin'));

        User::factory()
            ->count(3)
            // ->hasBlogposts(array_rand([0, 3, 4, 5]))
            ->hasBlogposts(5)
            ->create();

        // assign writer roles
        $writer = $userRoles->firstWhere('slug', 'writer');
        $writer->users()->attach(User::all());

        // guest
        $guest = new User();
        $guest->password = bcrypt('tyNC7J7W@VCwYbG');
        $guest->username = 'Gastje';
        $guest->name = 'Gast';
        $guest->email = 'guest@example.com';
        $guest->save();
    }
}
