<?php

namespace Database\Seeders;

use App\Models\Blogpost;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return Comment::factory()
            ->count(15)
            ->create();
    }
}
