<?php

namespace Database\Seeders;

use App\Models\Blogpost;
use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::factory()
            ->count(7)
            ->create();

        // create blogpost many-to-many relations

        $maxCategories = min(2, $categories->count());
        Blogpost::all()->each(function ($blogpost) use ($categories, $maxCategories) {
            $blogpost->categories()->attach(
                $categories->random(rand(1, $maxCategories))
            );
        });

        return $categories;
    }
}
