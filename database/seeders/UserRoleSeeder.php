<?php

namespace Database\Seeders;

use Database\Factories\UserRoleFactory;
use Illuminate\Database\Seeder;
use App\Models\UserRole;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'Admin', 'Writer',
        ];

        foreach ($roles as $role) {
            UserRole::updateOrCreate([
                'title' => $role
            ]);
        }
    }
}
