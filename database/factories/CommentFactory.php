<?php

namespace Database\Factories;

use App\Models\{Blogpost,Comment,User};
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $blogpostIds = Blogpost::all(['id']);
        $userIds = User::all(['id']);


        return [
            'blogpost_id' => $blogpostIds->random(),
            'user_id' => $userIds->random(),
            'body' => $this->faker->paragraph(10),
            'created_at' => $this->faker->dateTimeBetween('-1 months'),
        ];
    }
}
