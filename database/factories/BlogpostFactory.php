<?php

namespace Database\Factories;

use App\Models\{Blogpost,User};
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BlogpostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Blogpost::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'title' => $this->faker->sentence(4),
            // 'slug' => $this->faker->slug,
            'body' => $this->faker->paragraph(50),
            'image' => $this->faker->imageUrl(),
            'subscription_only' => $this->faker->boolean(25),
            'created_at' => $this->faker->dateTimeBetween('-3 months', '-1 months'),
        ];
    }
}
