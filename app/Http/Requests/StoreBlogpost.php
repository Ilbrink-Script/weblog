<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StoreBlogpost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && Auth::user()->isWriter();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|string|min:3|max:255',
            'image' => [
                'image',
                Rule::dimensions()
                    ->minWidth(480)->minHeight(320)
                    ->maxWidth(1920)->maxHeight(1080)
            ],
            'body' => 'required|string|min:10',
            'subscription_only' => 'boolean',
            'categories' => 'exists:categories,id',
        ];

        // require image to be uploaded upon creation only
        if ($this->getMethod() == 'POST') {
            array_push($rules['image'], 'required');
        }

        return $rules;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'subscription_only' => $this->request->has('subscription_only'),
        ]);
    }
}
