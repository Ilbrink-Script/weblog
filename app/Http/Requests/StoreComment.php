<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class StoreComment extends FormRequest
{
    public function __construct()
    {
        $this->redirect = URL::previous() . '#comment-form';
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'blogpost_id' => ['required', 'exists:App\Models\Blogpost,id'],
            // 'user_id' => ['required', 'exists:App\Models\User,id'],
            'body' => ['required', 'string', 'min:3'],
        ];
    }
}
