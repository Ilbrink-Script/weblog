<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::all()->orderBy('username');
        return $users;
    }

    public function show(User $user)
    {
        $blogposts = $user->blogposts()->paginate(5);
        return view('blogposts.index', compact(['blogposts', 'user']));
    }
}
