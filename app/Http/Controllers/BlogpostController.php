<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBlogpost;
use App\Models\Blogpost;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogpostController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Blogpost::class, 'blogpost');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $blogposts = Blogpost::withCount(['comments'])
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return view('blogposts.index', compact([
            'blogposts'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blogpost = new Blogpost();
        $categories = Category::all();
        return view('blogposts.create', compact(['blogpost', 'categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBlogpost $request)
    {
        $formData = $this->handleImageUpload($request, null);

        $blogpost = Blogpost::make($formData);
        $blogpost->user()->associate(Auth::user());
        $blogpost->save();

        if ($request->has('categories')) {
            $blogpost->categories()->attach($formData['categories']);
        }

        // redirect to the new blogpost
        return redirect(route('blogposts.show', $blogpost));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blogpost  $blogpost
     * @return \Illuminate\Http\Response
     */
    public function show(Blogpost $blogpost)
    {
        // $blogpost->loadCount(['categories', 'comments',]);
        return view('blogposts.show', compact([
            'blogpost'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blogpost  $blogpost
     * @return \Illuminate\Http\Response
     */
    public function edit(Blogpost $blogpost)
    {
        $categories = Category::all();
        return view('blogposts.edit', compact(['blogpost', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blogpost  $blogpost
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBlogpost $request, Blogpost $blogpost)
    {
        $formData = $this->handleImageUpload($request, $blogpost);

        $blogpost->update($formData); // fill + save

        if ($request->has('categories')) {
            $blogpost->categories()->sync($formData['categories']); // update categories
        } else {
            $blogpost->categories()->detach();
        }

        // redirect to the new blogpost
        return redirect(route('blogposts.show', $blogpost));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blogpost  $blogpost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blogpost $blogpost)
    {
        $blogpost->delete();
        return redirect(route('users.show', Auth::user()));
    }

    protected function handleImageUpload(Request $request, ?Blogpost $blogpost)
    {
        $formData = $request->validated();

        // store the image
        if ($request->has('image')) {
            $formData['image'] = $request->file('image')->store('blogposts', 'public');
        } else {
            $formData['image'] = optional($blogpost)->image;
        }

        return $formData;
    }
}
