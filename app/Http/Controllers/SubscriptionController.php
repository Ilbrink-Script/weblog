<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Models\User;
use Illuminate\Auth\Middleware\Authorize;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscriptions = Auth::user()->subscriptions;
        return view('subscriptions.index', compact('subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $author)
    {
        if (Gate::denies('create-subscription', $author)) {
            abort(403);
        }
        return view('subscriptions.create', compact('author'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $author)
    {
        if (Gate::denies('create-subscription', $author)) {
            abort(403);
        }

        Auth::user()->subscriptions()->attach($author);
        return redirect(route('users.show', $author));
    }

    public function destroy(Request $request, User $author)
    {
        if (Gate::denies('delete-subscription', $author)) {
            abort(403);
        }

        Auth::user()->subscriptions()->detach($author);

        return redirect(route('subscriptions.index'));
    }
}
