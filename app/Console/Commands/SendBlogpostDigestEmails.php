<?php

namespace App\Console\Commands;

use App\Mail\BlogpostDigest;
use App\Models\Blogpost;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendBlogpostDigestEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:digest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send the Blogpost Digest Emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $guests = User::doesntHave('userRoles')->get();

        if ($guests->count() === 0) {
            $this->info("No guest users found.");
            return 1; // no users found
        }

        $dateRange = [now()->subMonths(2), now()];
        $blogposts = Blogpost::orderBy('created_at', 'desc')
            ->whereBetween('created_at', $dateRange)
            ->limit(5) // don't make the e-mail too long
            ->get();

        if ($blogposts->count() === 0) {
            $this->info("No (recent) blogposts found.");
            return 1; // no (recent) blogposts
        }

        $guests->each(function($guest, $key) use ($blogposts) {
            Mail::to($guest)->send(new BlogpostDigest($guest, $blogposts));
            $this->info('Digest containing ' . $blogposts->count() . ' blogposts sent to ' . $guest->name . '.');
        });

        return 0;
    }
}
