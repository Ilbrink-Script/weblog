<?php

namespace App\Providers;

use App\Models\{Blogpost, Comment, User};
use App\Policies\{BlogpostPolicy, CommentPolicy};
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Blogpost::class => BlogpostPolicy::class,
        Comment::class => CommentPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('create-subscription', function(User $user, User $author) {
            return !$user->subscriptions->contains($author);
        });

        Gate::define('delete-subscription', function(User $user, User $author) {
            return $user->subscriptions->contains($author);
        });
    }
}
