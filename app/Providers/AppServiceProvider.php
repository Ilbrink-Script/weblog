<?php

namespace App\Providers;

use App\Models\Category;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // view composer
        View::composer(['categories.sidebar', 'categories.filter'], function ($view) {
            $view->with('categories', Category::withCount(['blogposts'])->orderBy('title')->get());
        });

    }
}
