<?php

namespace App\Policies;

use App\Models\Blogpost;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BlogpostPolicy
{
    use HandlesAuthorization;

    // let me do everything
    public function before(?User $user, $ability)
    {
        if (optional($user)->isAdmin()) {
            // not yet
            // return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(?User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Blogpost  $blogpost
     * @return mixed
     */
    public function view(?User $user, Blogpost $blogpost)
    {
        if ($blogpost->user->is($user))
            return true;

        if ($blogpost->subscription_only) {
            return $blogpost->user->subscribers->contains($user);
        }

        return !$blogpost->subscription_only;

        // do something to check if user has a subscription to blogpost author
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isWriter();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Blogpost  $blogpost
     * @return mixed
     */
    public function update(User $user, Blogpost $blogpost)
    {
        return $blogpost->user->is($user);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Blogpost  $blogpost
     * @return mixed
     */
    public function delete(User $user, Blogpost $blogpost)
    {
        return $blogpost->user->is($user);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Blogpost  $blogpost
     * @return mixed
     */
    public function restore(User $user, Blogpost $blogpost)
    {
        return $blogpost->user->is($user);
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Blogpost  $blogpost
     * @return mixed
     */
    public function forceDelete(User $user, Blogpost $blogpost)
    {
        return $blogpost->user->is($user);
    }
}
