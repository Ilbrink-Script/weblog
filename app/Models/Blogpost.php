<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Blogpost extends Model
{
    use HasFactory, Sluggable;

    protected $fillable = [
        'title',
        'image',
        'body',
        'subscription_only',
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class)->orderBy('created_at', 'desc'); // newest first?
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class)->withTimestamps()->orderBy('title');
    }

    public function getPath()
    {
        return route('blogposts.show', $this);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function getImageAsset()
    {
        if (filter_var($this->image, FILTER_VALIDATE_URL)) {
            return asset($this->image);
        }

        return asset(implode('/', ['storage', $this->image]));
    }
}
