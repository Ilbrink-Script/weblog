<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userRoles()
    {
        return $this->belongsToMany(UserRole::class);
    }

    public function blogposts()
    {
        return $this->hasMany(Blogpost::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function blogpostComments()
    {
        return $this->hasManyThrough(Comment::class, Blogpost::class);
    }

    public function subscribers()
    {
        return $this->belongsToMany(User::class, 'subscriptions', 'author_id', 'user_id')->withTimestamps();
    }

    public function subscriptions()
    {
        return $this->belongsToMany(User::class, 'subscriptions', 'user_id', 'author_id')->withTimestamps();
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    protected function hasUserRole(String $requestedRole)
    {
        foreach ($this->userRoles as $userRole) {
            if ($userRole->slug === $requestedRole) {
                return true;
            }
        }
        return false;
    }

    public function isWriter()
    {
        return $this->hasUserRole('writer');
    }

    public function isAdmin()
    {
        return $this->hasUserRole('admin');
    }

    public function isGuest()
    {
        return count($this->userRoles) === 0;
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'username'
            ]
        ];
    }
}
