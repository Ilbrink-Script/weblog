<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BlogpostDigest extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $blogposts;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Collection $blogposts)
    {
        $this->user = $user;
        $this->blogposts = $blogposts;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.blogposts.digest');
    }
}
